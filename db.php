<?php
$host = 'localhost';
$db = 'wieg16-curl';
$user = 'root';
$password = 'root';
$charset = 'utf8';
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [ PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
PDO::ATTR_EMULATE_PREPARES   => false  ];

$pdo = new PDO($dsn, $user, $password, $options);



if ( isset($_POST['submit']) ) {
$sql = "INSERT INTO `form` (`firstname`, `lastname`) VALUES (:firstname, :lastname)";
$stm_insert = $pdo->prepare($sql);
$stm_insert->execute(['firstname' => $_POST['firstname'], 'lastname' => $_POST['lastname']]);
}

