<?php

function get_webpage($url) {

  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HEADER, 0);

 $response =  curl_exec($ch);
  curl_close($ch);

  return $response;
}

function count_words($text, array $words) {
  $text   = strip_tags($text);
  $text   = strtolower($text);
  $result = [];
  foreach( $words as $word => $value) {
          $number= substr_count($text, $word, 0);
          $result[] =  $word . " " .  $number;
  }
    return $result;
};

$wordSearch = [
  "hej" => 0,
  "ipsum" => 0,
  "lorem" => 0
];

$page = get_webpage("http://www.catipsum.com/");


?>
<ul>
  <?php foreach (count_words($page, $wordSearch) as $wordSearch): ?>
    <li><?= $wordSearch ?></li>
  <?php endforeach; ?>
</ul>
